import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UsersModule } from './users/users.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'postgre',
      database: 'localhost',
      entities: ['dist/**/*.entity{.ts,.js}'],
     // synchronize: false,
      retryDelay:3000,
      retryAttempts:10
    }),

    UsersModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
