import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { UsersService } from '../services/users.service';

@Controller('api/users')
export class UsersController {

    constructor(
        private usersService : UsersService
    ){}
    @Get()   
    getAll(){   
        return this.usersService.findAll();
    }

    @Get(':id')
    getOne(@Param('id') id: number){
        return this.usersService.findOne(id);

    }

    @Post()
    create(@Body() body:any){
        return this.usersService.create(body);
        
    }

    @Put(':id')
    update(@Param('id') id: number, @Body() body:any) {
        return this.usersService.update(id,body);
    } 

    @Delete(':id')
    delete(@Param('id') id: number ){
        return this.usersService.delete(id);
        
    } 

}
