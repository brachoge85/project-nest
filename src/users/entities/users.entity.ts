import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm'


@Entity()
export class Tbluser{
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    nombre:string;

    @Column({default: ''})
    direccion:string;

    @Column({default:''})
    email:string;

    @Column({type:'timestamp', default:()=>"CURRENT_TIMESTAMPS"})
    fecha_nacimiento:Date;

    @Column()
    edad:number;

    @Column()
    rut:number;


}