import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Tbluser } from '../entities/users.entity';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(Tbluser)
        private userRepository: Repository<Tbluser>,
      ) {}
    
      async findAll() {
        return this.userRepository.find();
      }

      findOne(id:number) {
        return this.userRepository.findOne(id);
      }

      create(body: any) {
        const newUser= this.userRepository.create(body);
        return this.userRepository.create(newUser);
      }

     async update(id:number, body:any) {
        const user= await this.userRepository.findOne(id);
        this.userRepository.merge(user, body);
        return this.userRepository.save(user);
      }

      delete(id: number) {
        return this.userRepository.delete(id);
      }



      




}



