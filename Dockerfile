FROM node:14-alpine
ARG NODE_ENV


# RUN mkdir -p /nestjs/src/dist/

# WORKDIR /nestjs/src/dist/
COPY dist ./
COPY package*.json ./
#COPY newrelic.js ./
# COPY temp ./

RUN npm install --production
RUN ls -lt

EXPOSE 8080
CMD ["node", "main.js"]

